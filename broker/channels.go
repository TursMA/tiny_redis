package main


import (
	"log"
	
	"config"
	"structs"
)


/*
Struct for holding channels for one go routine.
*/
type EntryChannel struct {
	RequestEntryChannel chan structs.Entry
	CmdChannel chan uint8
	ResponseEntryChannel chan []structs.Entry
}


/*
Global storage for all channels.
*/
var EntryChannels []EntryChannel 


/*
Inits channels and node connections.
*/
func InitChannels() {
	for nodeID, nodeAddress := range config.Config.Nodes {
		log.Printf("Add new channel: %s\n", nodeAddress)
		EntryChannels = append(EntryChannels, 
			EntryChannel{make(chan structs.Entry), make(chan uint8), make(chan []structs.Entry)}) // no buffer
		
		// Handles connections with nodes.
		//TODO: Performance issue. Maybe it's better to create go routing on each http type request instead using one queue. 
		go Dispatch(nodeID, nodeAddress)
	}
}
