package main


import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	
	"github.com/gorilla/mux"
	"structs"
)


type jsonErr struct {
	Code int    `json:"code"`
	Text string `json:"text"`
}


/*
Sets response status.
*/
func setResponseStatus(status int, info interface{}, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(status)
	err := json.NewEncoder(w).Encode(info)
	if err != nil {
		panic(err)
	}
}


/*
Processes data provider result and sends response.
*/
func processDataProviderResult(entry structs.Entry, w http.ResponseWriter) {
	if len(entry.Key) == 0 {
		// If we didn't find it, 404
		setResponseStatus(http.StatusNotFound, jsonErr{Code: http.StatusNotFound, Text: "Not Found"}, w)	
		return
	}

	setResponseStatus(http.StatusOK, entry, w)	
}


/*
Reads json into provided struct
*/
func readBody(entry interface{}, w http.ResponseWriter, r *http.Request) bool {
	// Reading body.
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		panic(err)
	}
	
	err = r.Body.Close();
	if err != nil {
		panic(err)
	}
	
	err = json.Unmarshal(body, entry);
	if err != nil {
		setResponseStatus(422, err, w)   // unprocessable entity
		return false
	}
	return true
}


/*
curl -vXPOST http://localhost:42586/add -H "Content-Type: application/json" -d '{"key":"key1", "value":"1234",
		"props": {"TTL":5, "persistent":true}}' 
*/
func AddEntry(w http.ResponseWriter, r *http.Request) {
	var entry structs.Entry 

	if !readBody(&entry, w, r) {
		return
	}

	if len(entry.Key) == 0 {
		setResponseStatus(http.StatusNotAcceptable, jsonErr{Code: http.StatusNotAcceptable, Text: "Invalid key"}, w)
		return
	}

	entry = ForwardEntryCmd(entry, structs.CMD_ADD)[0]
	//TODO: bug. Replace 404 with correct error.
	processDataProviderResult(entry, w)
}


/*
curl -vXGET http://localhost:42586/show/{key}
*/
func ShowEntry(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	entry := structs.Entry{}
	entry.Key = vars["Key"]
	entry = ForwardEntryCmd(entry, structs.CMD_SHOW)[0]
	processDataProviderResult(entry, w)
}


/*
curl -vXGET http://localhost:42586/showkeys
*/
func ShowKeys(w http.ResponseWriter, r *http.Request) {
	entries := ForwardEntryCmd(structs.Entry{}, structs.CMD_KEYS)
	keys := []string{}
	for _, entry := range entries {
		keys = append(keys, entry.Key)	
	}
	setResponseStatus(http.StatusOK, keys, w)
}


/*
curl -vXPUT http://localhost:42586/show/{key} -H "Content-Type: application/json" -d '{"key":"key1", "value":"12345",
		"props": {"TTL":5, "persistent":true}}'
*/
func FullUpdateEntry(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var entry structs.Entry 
	if !readBody(&entry, w, r) {
		return
	}
	
	resKey := vars["Key"]
	
	if resKey != entry.Key {
		setResponseStatus(http.StatusNotAcceptable, jsonErr{Code: http.StatusNotAcceptable, Text: "Keys mismatch"}, w)
		return	
	}
	
	//TODO: checks presence of all keys in incoming dict.
	entry = ForwardEntryCmd(entry, structs.CMD_UPD)[0]
	processDataProviderResult(entry, w)
}


/*
curl -vXDELETE http://localhost:42586/delete/{key}
TODO: consider batch deletion via json.
*/
func DeleteEntry(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	entry := structs.Entry{}
	entry.Key = vars["Key"]
	entry = ForwardEntryCmd(entry, structs.CMD_DEL)[0]
	processDataProviderResult(entry, w)
}
