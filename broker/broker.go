package main


import (
	"log"
	"fmt"
	"net/http"

	"config"
)


func main() {
 	config.Init()
	InitChannels()
	InitRateLogger()
	router := NewRouter()
	log.Println("HttpBroker started...")
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", config.Config.ExternalPort), router))
}
