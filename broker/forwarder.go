package main

import (
	"hash/crc32"
	"log"
	
	"config"
	"structs"
)

/*
Calculates node ID from key.
*/
func calcNodeID(key string) uint32 {
	// TODO: bug. Performance issue. Replace with unix sum8
	return crc32.ChecksumIEEE([]byte(key)) % uint32(len(config.Config.Nodes))
}


func forwardEntryCmdImpl(entry structs.Entry, cmd uint8, nodeID uint32) []structs.Entry {
	entryChannel := EntryChannels[nodeID]

	log.Printf("Forward entry %s to node %d, cmd %d\n", entry.Key, nodeID, cmd)

	// Order is important because of deadlock.
	entryChannel.CmdChannel <- cmd	
	entryChannel.RequestEntryChannel <- entry
	
	log.Printf("Forwarder is wainting for response add entry %s to node %d\n", entry.Key, nodeID)
	entries := <- entryChannel.ResponseEntryChannel

	log.Printf("Forwarder has received response add entry %s to node %d\n", entry.Key, nodeID)

	return entries
}


func ForwardEntryCmd(entry structs.Entry, cmd uint8) []structs.Entry {
	var nodeIDs []uint32
	switch cmd {
		case structs.CMD_KEYS:
			for i, _ := range(config.Config.Nodes) {
				nodeIDs = append(nodeIDs, uint32(i))
			}	
		default:
			nodeIDs = append(nodeIDs, calcNodeID(entry.Key))
	}
	
	// Combines results
	results := []structs.Entry{}
	for _, nodeID := range nodeIDs {
		results = append(results, forwardEntryCmdImpl(entry, cmd, uint32(nodeID))...)
	}
	
	return results
	
}
