package main


import (
	"net/http"
	
	"github.com/gorilla/mux"
)


// Return multiplexor.
func NewRouter() *mux.Router {

	router := mux.NewRouter()
	router.StrictSlash(true)

	for _, route := range routes {
		var HandlerFunc http.Handler
		
		HandlerFunc = route.HandlerFunc
		// Overrides handler function with logger.
		HandlerFunc = wrapByLogger(HandlerFunc, route.Name)
		
		// Adds route to router.
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(HandlerFunc)
	}

	return router
}
