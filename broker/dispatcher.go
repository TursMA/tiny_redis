package main


import (
	"fmt"
	"bytes"
	"log"
	"encoding/gob"
	"net"
	"time"
	"io"
	
	"structs"
)


func makeNodeTCPConn(nodeID int, address string) net.Conn {
	// Dial TCP connection.
	tcpAddr, err := net.ResolveTCPAddr("tcp", address)
	if err != nil {
		panic(err)
	}
	
	var conn net.Conn
	
	// Waiting for a node connection.
	for {
		log.Printf("Connecting to node %d...\n", nodeID)
		conn, err = net.DialTCP("tcp", nil, tcpAddr)
		if err != nil {
			log.Printf("Dial to node %d failed %s:%s\n", nodeID, address, err.Error())
		} else {
			conn.SetReadDeadline(time.Now().Add(10 * time.Second))
			//conn.SetWriteDeadline(time.Now().Add(10 * time.Second))
			break
		}
		time.Sleep(5 * time.Second)
	}
	return conn
}


func ensureConnection(conn net.Conn, nodeID int) bool {
	conn.SetReadDeadline(time.Now())
	one := make([]byte, 1)
	if _, err := conn.Read(one); err == io.EOF {
		log.Fatal(fmt.Sprintf("Connection to node %d is lost: %s", nodeID, err.Error()))
		conn.Close()
		return false
	} else {
		conn.SetReadDeadline(time.Now().Add(10 * time.Second))
	}
	log.Printf("Node %d connection is ok...\n", nodeID)
	return true
}


func writeToNode(conn net.Conn, outBuf *bytes.Buffer, nodeID int) bool {
	_, err := conn.Write([]byte(outBuf.String()))
	if err != nil {
		log.Printf("Write request to node %d failed: %s", nodeID, err.Error())
		return false
	}
	return true
}


func readFromNode(conn net.Conn, inBuf *bytes.Buffer, nodeID int) bool {
	//TODO: bug. Need read all.
	//TODO: Performance issue. Need to use buffer pool instead of allocation.
	buf := make([]byte, 2048)
	_, err := conn.Read(buf)
	if err != nil {
		log.Printf("Read response from node %d failed: %s", nodeID, err.Error())
		return false
	}
	inBuf.Write(buf)
	return true
}


func Dispatch(nodeID int, address string) {
	entryChannel := EntryChannels[nodeID]

	// Serializators.
	var outBuf, inBuf bytes.Buffer
	enc := gob.NewEncoder(&outBuf)
	dec := gob.NewDecoder(&inBuf)


	// Waiting for a node connection.
	conn := makeNodeTCPConn(nodeID, address) 

	// Main cycle.
	for {
		log.Printf("------->>>Node %d is waiting for command...\n", nodeID)

		cmd := <-entryChannel.CmdChannel	
		entry := <-entryChannel.RequestEntryChannel
		
		//TODO: Performance issue. What about batching?		
		
		inBuf.Reset()
		outBuf.Reset()
		
		log.Printf("Node %d has received command %d and entry %s\n", nodeID, cmd, entry.Key)
		
		err := enc.Encode(structs.NetworkEntry{[]structs.Entry{entry}, cmd})
		if err != nil {
			log.Printf("Encode error, node %d failed: %s", nodeID, err.Error())
			entryChannel.ResponseEntryChannel <- []structs.Entry{}
			continue
		}
		
		if !ensureConnection(conn, nodeID) {	
			conn = makeNodeTCPConn(nodeID, address)
		}

		log.Printf("Node %d is writing now...\n", nodeID)
		if !writeToNode(conn, &outBuf, nodeID) {
			entryChannel.ResponseEntryChannel <- []structs.Entry{}
			continue
		}

		log.Printf("Node %d is reading now...\n", nodeID)
		if !readFromNode(conn, &inBuf, nodeID) {
			entryChannel.ResponseEntryChannel <- []structs.Entry{}
			continue
		}

		var networkEntry structs.NetworkEntry
		err = dec.Decode(&networkEntry)
		if err != nil {
			log.Printf("Decode error, node %d failed: %s", nodeID, err.Error())
			entryChannel.ResponseEntryChannel <- []structs.Entry{}
			continue
		}
		
		log.Printf("Node %d is sending back responce for command %d\n", nodeID, cmd)

		entryChannel.ResponseEntryChannel <- networkEntry.Data  
	} 
}
