package main


import (
	"log"
	"net/http"
	"time"
)


var EMA float64
var NumberOfCalls uint64


func InitRateLogger() {
	NumberOfCalls = 0
	EMA = 0
 
	go func() {
		for {
			time.Sleep(time.Second)
			EMA = (float64(7) * EMA + float64(NumberOfCalls))/ float64(8)
			NumberOfCalls = 0 
			log.Printf("--------------->>>>>>>>>>>>>>>>>>Rate %f\n", EMA)
		}
	}()
}


func wrapByLogger(handler http.Handler, name string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		// Allows us to remain compatible with net/http.
		handler.ServeHTTP(w, r)

		log.Printf(
			"%s\t%s\t%s\t%s",
			r.Method,
			r.RequestURI,
			name,
			time.Since(start),
		)
		
		NumberOfCalls++
	})
}
