package main


import (
	"net/http"
)


type Route struct {
	Name		string
	Method		string
	Pattern		string
	HandlerFunc	http.HandlerFunc
}


type Routes []Route


var routes = Routes{
	Route{
		"add",
		"POST",
		"/add",
		AddEntry,
	},
	Route{
		"del",
		"DELETE",
		"/delete/{Key}",
		DeleteEntry,
	},
	Route{
		"update",
		"PUT",
		"/update/{Key}",
		FullUpdateEntry,
	},
	Route{
		"show",
		"GET",
		"/show/{Key}",
		ShowEntry,
	},
	Route{
		"showkeys",
		"GET",
		"/showkeys",
		ShowKeys,
	},

}
