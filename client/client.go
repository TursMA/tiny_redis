package main


import (
	"net/http"
	"log"
	"fmt"
	"bytes"
	"io/ioutil"
	"sync"
	"time"
	
	"config"
)


var NumberOfResponses uint64


/*
Does a simple HTTP call.
*/
func doRequest(cmd string, url string, query string, wg *sync.WaitGroup) {
	log.Println("URL", url)

	var jsonStr = []byte(query)
	req, err := http.NewRequest(cmd, url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	log.Println("response Status:", resp.Status)
//	log.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	log.Println("response Body:", string(body))
	NumberOfResponses++
	wg.Done()
}


func main() {
	log.Println("Client started...")

	config.Init()
	url := fmt.Sprintf("http://%s:%d/", config.Config.ClientHostName, config.Config.ExternalPort)

	NumberOfResponses = 0
	start := time.Now()

	var iter = 0
	var wg sync.WaitGroup
	for {
		iter++
		time.Sleep(time.Millisecond)
		for _, ri := range GetCommands() {
			wg.Add(1)
			// Race conditions!
			go doRequest(ri.Cmd, url + ri.Sub, ri.Query, &wg)
		}

		rate := float64(NumberOfResponses * 1000000000) / float64(time.Since(start))
		log.Printf("------->>>Responses %d, Elapsed time %s, Rate %f\n", NumberOfResponses, time.Since(start), rate)
		
		if iter > 0 {
			break
		}
	}
	
	rate := float64(NumberOfResponses * 1000000000) / float64(time.Since(start))
	log.Printf("------->>>Responses %d, Elapsed time %s, Rate %f\n", NumberOfResponses, time.Since(start), rate)
	log.Println("Client finished...")
	
	// Wait for all HTTP fetches to complete.
	wg.Wait()
}
