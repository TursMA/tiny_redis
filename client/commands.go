package main


type RequestInfo struct {
	Cmd		string
	Query	string
	Sub		string
}


func GetCommands() []RequestInfo {
	return []RequestInfo {
		{"POST", `{"key":"key1", "value":"1234", "props": {"TTL":5, "persistent":true}}`, "add"},
		//{"POST", `{"key":"key2", "value":"1234", "props": {"TTL":5, "persistent":true}}`, "add"},
		//{"POST", `{"key":"key3", "value":"1234", "props": {"TTL":5, "persistent":true}}`, "add"},
		//{"POST", `{"key":"key4", "value":"1234", "props": {"TTL":5, "persistent":true}}`, "add"},
		{"POST", `{"key":"key5", "value":"1234", "props": {"TTL":5, "persistent":true}}`, "add"},
		
/*		{"PUT", `{"key":"key1", "value":"1234c", "props": {"TTL":5, "persistent":true}}`, "update/key1"},
		{"PUT", `{"key":"key2", "value":"1234c", "props": {"TTL":5, "persistent":true}}`, "update/key2"},
		{"PUT", `{"key":"key3", "value":"1234c", "props": {"TTL":5, "persistent":true}}`, "update/key3"},
		{"PUT", `{"key":"key4", "value":"1234c", "props": {"TTL":5, "persistent":true}}`, "update/key4"},
		{"PUT", `{"key":"key5", "value":"1234c", "props": {"TTL":5, "persistent":true}}`, "update/key5"},
		
		{"DELETE", ``, "delete/key1"},
		{"DELETE", ``, "delete/key2"},
		{"DELETE", ``, "delete/key3"},
		{"DELETE", ``, "delete/key4"},
		{"DELETE", ``, "delete/key5"},
		
		{"GET", ``, "show/key1"},
		{"GET", ``, "show/key2"},
		{"GET", ``, "show/key3"},
		{"GET", ``, "show/key4"},
		{"GET", ``, "show/key5"},

		{"GET", ``, "showkeys"},
		{"GET", ``, "showkeys"},
		{"GET", ``, "showkeys"},
		{"GET", ``, "showkeys"},*/
		{"GET", ``, "showkeys"},
	}
}
