package structs


/*
Properties of the entry.
*/
type Properties struct {
	Persistent	bool				`json:"persistent"`
	TTL			int					`json:"TTL"`
}


/*
Key, value storage.
*/
type Entry struct {
	Key			string				`json:"key"`
	Value		string				`json:"value"`
	Props		Properties			`json:"props"`
}



type NetworkEntry struct {
	Data		[]Entry
	Cmd			uint8	//One of CMD_*.		
}


const ( 
       CMD_ADD 		= 1
       CMD_SHOW 	= 2
       CMD_DEL		= 3
       CMD_UPD		= 4
       CMD_KEYS		= 5
    )
