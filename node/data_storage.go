package main


import (
	"sync"
	"time"
	"fmt"
	"encoding/gob"
	"io"
	"os"
	
    "structs"
)

//TODO: bug. Cache should be more abstract.

const INFINITE = int64(0)


type StorageItem struct {
	Item structs.Entry
	Expiration int64
	Persistent bool
}


/*
Maps are not safe for concurrent use.
*/
type ProtectedStorage struct {
	//TODO: Performance issue. Mutex vs channel?
    sync.RWMutex
    storage map[string]StorageItem
}

/*
Global data storage.
*/
var GStorage *ProtectedStorage


/*
Inits global storage.
*/
func InitStorage(nodeID int) {
	GStorage = New()
	GStorage.LoadFile(getDBName(nodeID))
}


func New() *ProtectedStorage {
	return &ProtectedStorage{storage: make(map[string]StorageItem)}
}


func (ps *ProtectedStorage) addImpl(entry structs.Entry) structs.Entry {
	// TTL logic.
	expiration := INFINITE
	ttl := int64(entry.Props.TTL)
	if ttl != INFINITE {
		 expiration = time.Now().Add(time.Duration(ttl) * time.Second).UnixNano()
	}
	
	persistent := entry.Props.Persistent
	ps.storage[entry.Key] = StorageItem{entry, expiration, persistent}
	return entry
}


func (ps *ProtectedStorage) Add(entry structs.Entry) structs.Entry {
	ps.Lock()
	defer ps.Unlock()
	
	return ps.addImpl(entry)
}


func (ps *ProtectedStorage) IsExpired(entry structs.Entry) bool {
	item, _ := ps.storage[entry.Key]
	if item.Expiration == INFINITE {
		return false
	}
	return time.Now().UnixNano() > item.Expiration
}


func (ps *ProtectedStorage) Get(entry structs.Entry) structs.Entry {
	ps.RLock()
	defer ps.RUnlock()

	item, ok := ps.storage[entry.Key]
	if ok && !ps.IsExpired(entry) {
		return item.Item
	}
	return structs.Entry{}
}


func (ps *ProtectedStorage) Set(entry structs.Entry) structs.Entry {
	ps.Lock()
	defer ps.Unlock()

	_, ok := ps.storage[entry.Key]
	if ok && !ps.IsExpired(entry) {
		return ps.addImpl(entry)
	}
	return structs.Entry{}
}


func (ps *ProtectedStorage) Del(entry structs.Entry) structs.Entry {
	ps.Lock()
	defer ps.Unlock()

	return ps.delImpl(entry)
}


func (ps *ProtectedStorage) Keys() []structs.Entry {
	ps.RLock()
	defer ps.RUnlock()

	keys := []structs.Entry{}
	for _, item := range ps.storage {
		if !ps.IsExpired(item.Item) {
			keys = append(keys, structs.Entry{item.Item.Key, "", item.Item.Props})
			if len(keys) == 100 {
				break		
			}
		}
	}
	return keys
}


func (ps *ProtectedStorage) delImpl(entry structs.Entry) structs.Entry {
	item, ok := ps.storage[entry.Key]
	if ok {
		delete(ps.storage, entry.Key)
		return item.Item
	}
	return structs.Entry{}
}


func (ps *ProtectedStorage) Save(w io.Writer) (err error) {
	ps.RLock()
	defer ps.RUnlock()
	
	enc := gob.NewEncoder(w)
	//TODO: Performance issue. Mem alloc.
	tmpList := []StorageItem{}
	for _, item := range ps.storage {
		if item.Persistent && !ps.IsExpired(item.Item) {
			tmpList = append(tmpList, item)
		}	
	}
	return enc.Encode(&tmpList)
}


func (ps *ProtectedStorage) SaveFile(fname string) error {
	fp, err := os.Create(fname)
	if err != nil {
		return err
	}
	err = ps.Save(fp)
	if err != nil {
		fp.Close()
		return err
	}
	return fp.Close()
}


func (ps *ProtectedStorage) Load(r io.Reader) error {
	dec := gob.NewDecoder(r)
	storage := []StorageItem{}
	err := dec.Decode(&storage)
	if err == nil {
		ps.Lock()
		defer ps.Unlock()
		for _, item := range storage {
			foundItem, ok := ps.storage[item.Item.Key]
			if !ok || ps.IsExpired(foundItem.Item) {
				fmt.Printf("Item from storage loaded %s", item.Item.Key)
				ps.storage[item.Item.Key] = item
			}
		}
	}
	return err
}


func (ps *ProtectedStorage) LoadFile(fname string) error {
	fp, err := os.Open(fname)
	if err != nil {
		return err
	}
	err = ps.Load(fp)
	if err != nil {
		fp.Close()
		return err
	}
	return fp.Close()
}
