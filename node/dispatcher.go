package main


import (
	"net"
	"bytes"
	"log"
	"encoding/gob"
	
	"structs"
)


func readFromNode(conn net.Conn, inBuf *bytes.Buffer, nodeID int) bool {
	//TODO: bug. Need read all.
	//TODO: Performance issue. Need to use buffer pool instead of allocation.
	buf := make([]byte, 2048)
	_, err := conn.Read(buf)
	if err != nil {
		log.Printf("Read response from node %d failed: %s", nodeID, err.Error())
		return false
	}
	inBuf.Write(buf)
	return true
}


func writeToNode(conn net.Conn, outBuf *bytes.Buffer, nodeID int) bool {
	_, err := conn.Write([]byte(outBuf.String()))
	if err != nil {
		log.Printf("Write request to node %d failed: %s", nodeID, err.Error())
		return false
	}
	return true
}


func Dispatch(conn net.Conn, nodeID int) {
	defer conn.Close()
	
	// Serializators.
	var outBuf, inBuf bytes.Buffer
	enc := gob.NewEncoder(&outBuf)
	dec := gob.NewDecoder(&inBuf)

	for {
	
		log.Printf("Node %d is reading now...\n", nodeID)

		outBuf.Reset()
		inBuf.Reset()

		if !readFromNode(conn, &inBuf, nodeID) {
			break
		}

		var networkEntry structs.NetworkEntry
		err := dec.Decode(&networkEntry)
		if err != nil {
			log.Printf("Decode error, node %d failed: %s", nodeID, err.Error())
			break
		}

		log.Printf("Node %d has read: %s\n", nodeID, networkEntry.Data[0].Key)
		
		entries :=[]structs.Entry{}

		switch networkEntry.Cmd {
			case structs.CMD_ADD:
				entries = append(entries, GStorage.Add(networkEntry.Data[0]))
			case structs.CMD_SHOW:
				entries = append(entries, GStorage.Get(networkEntry.Data[0]))
			case structs.CMD_DEL:
				entries = append(entries, GStorage.Del(networkEntry.Data[0]))
			case structs.CMD_UPD:
				entries = append(entries, GStorage.Set(networkEntry.Data[0]))
			case structs.CMD_KEYS:
				entries = GStorage.Keys()
		}

		networkEntry.Data = entries

		err = enc.Encode(networkEntry)
		if err != nil {
			log.Printf("Encode error, node %d failed: %s", nodeID, err.Error())
			break
		}

		log.Printf("Node %d is writing now: %s\n", nodeID)

		writeToNode(conn, &outBuf, nodeID)
		if err != nil {
			log.Printf("Encode error, node %d failed: %s", nodeID, err.Error())
			break
		}
	}
}
