package main


import (
	"fmt"
	"net"
	"os"
	"strings"
	"strconv"
	"log"

	"config"
)


func makeTCPListener(nodeID int, address string) *net.TCPListener {
	tcpAddr, err := net.ResolveTCPAddr("tcp", address)
	if err != nil {
		panic(err)
	}

	listener, err := net.ListenTCP("tcp", tcpAddr)
	if err != nil {
		panic(err)
	}

	return listener
}


func getDBName(nodeID int) string {
	return fmt.Sprintf("db_node_%d", nodeID)
}


func main() {
	if len(os.Args) < 2 {
		log.Printf("I: syntax: %s <nodeID>\n", os.Args[0])
		return
	}
	
	nodeID, err := strconv.Atoi(os.Args[1])
	if err != nil {
		panic(err)
	}

	config.Init()

	if nodeID >= len(config.Config.Nodes) {
		log.Printf("Invalid node id %d. Check config for details", nodeID)
	}
	
	InitStorage(nodeID)
	IdleSaveData(nodeID)
	IdleExpiryData(nodeID)

	listener := makeTCPListener(nodeID, fmt.Sprintf("0.0.0.0:%s",
			strings.Split(config.Config.Nodes[nodeID], ":")[1]))

	defer listener.Close()

	log.Printf("Node %d is running now...\n", nodeID)

	for {
		// Listen for an incoming connection.
		conn, err := listener.Accept()
		if err != nil {
			panic(err)
		}
		// Handle connections in a new goroutine.
		go Dispatch(conn, nodeID)
	}
}
