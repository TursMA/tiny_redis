package main


import (
	"testing"
	"os"
	"time"
	
	"structs"
)


func setup() {
	InitStorage(0)
}


func TestAddGet(t *testing.T) {
	entry := structs.Entry{"1", "1", structs.Properties{false, 0}}
	GStorage.Add(entry)
	if GStorage.Get(entry) != entry {
		t.Error("Entry is not in cache!")
	}
}


func TestAddGetTTL(t *testing.T) {
	entry := structs.Entry{"2", "1", structs.Properties{false, 1}}
	GStorage.Add(entry)
	//TODO: bug. Tests must not suspend threads.
	time.Sleep(2*time.Second)
	if GStorage.Get(entry) == entry {
		t.Error("TTL doesn't work!")
	}
}


func TestGetNothing(t *testing.T) {
	entry := structs.Entry{"3", "1", structs.Properties{false, 0}}
	if GStorage.Get(entry).Key != "" {
		t.Error("Empty entry is expected!")
	}
}


func TestSetSuccess(t *testing.T) {
	entry := structs.Entry{"1", "2", structs.Properties{false, 0}}
	GStorage.Set(entry)
	if GStorage.Get(entry) != entry {
		t.Error("Entry is not updated!")
	}
}


func TestSetTTL(t *testing.T) {
	entry := structs.Entry{"2", "2", structs.Properties{false, 1}}
	upEntry := GStorage.Set(entry)
	if upEntry == entry {
		t.Error("Entry is updated!")
	}
}


func TestSetFailure(t *testing.T) {
	entry := structs.Entry{"3", "2", structs.Properties{false, 0}}
	upEntry := GStorage.Set(entry)
	if upEntry.Key != "" {
		t.Error("Entry is updated!")
	}
}


func TestDelFailure(t *testing.T) {
	entry := structs.Entry{"3", "2", structs.Properties{false, 0}}
	delEntry := GStorage.Del(entry)
	if delEntry.Key != "" {
		t.Error("Entry is deleted!")
	}
}


func TestDelSuccess(t *testing.T) {
	entry := structs.Entry{"1", "2", structs.Properties{false, 0}}
	delEntry := GStorage.Del(entry)
	if delEntry != entry {
		t.Error("Entry is not deleted!")
	}
}



func TestMain(m *testing.M) {
	setup()
	ret := m.Run()
	os.Exit(ret)
}

