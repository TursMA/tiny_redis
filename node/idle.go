package main


import (
	"time"
	"log"
	
	"config"
)


func IdleSaveData(nodeID int) {

	go func () {
		for {
	
			time.Sleep(time.Second * time.Duration(config.Config.DiskIOTimeout))
			
			log.Print("---->>>Idle save data is running...")
			
			//TODO: Performance issue. Blocks all write operarions...
			GStorage.SaveFile(getDBName(nodeID))
			
			log.Print("---->>>Idle save data finished")
		}
	}()
}


func IdleExpiryData(nodeID int) {
	
	go func() {
		for {
		
			time.Sleep(time.Second * time.Duration(config.Config.ExpiryTimeout))
			
			log.Print("---->>>Idle expiry data is running...")
			
			func () {
				//TODO: Performance issue. Blocks all operarions...
				GStorage.Lock()
				defer GStorage.Unlock()
				for _, item := range GStorage.storage {
					if GStorage.IsExpired(item.Item) {
						log.Printf("Delete expired entry %s", item.Item.Key)
						GStorage.delImpl(item.Item)
					}	
				}
			}()
			
			log.Print("---->>>Idle expiry data finished")
		}
	}()
}
