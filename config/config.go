package config


import (
	"encoding/json"
	"os"
	"runtime"
	"path"
	"log"
)

type Configuration struct {
	ExternalPort	int			`json:"ExternalPort"`
	ClientHostName	string		`json:"ClientHostName"`
	Nodes			[]string	`json:"nodes"`
	DiskIOTimeout	int			`json:"diskiotimeout"`
	ExpiryTimeout	int			`json:"expirytimeout"`
}

var Config Configuration

func Init() {
	_, filename, _, _ := runtime.Caller(1)
	file, err := os.Open(path.Join(path.Dir(path.Dir(filename)), "config.json"))
	if err != nil {
		panic(err)
	}
	defer file.Close() 
	
	err = json.NewDecoder(file).Decode(&Config)
	if err != nil {
		panic(err)
	}
	log.Printf("ExternalPort:%d\n", Config.ExternalPort)
	log.Printf("ClientHostName:%s\n", Config.ClientHostName)
	log.Printf("Nodes:%s\n", Config.Nodes)
	log.Printf("DiskIOTimeout:%d\n", Config.DiskIOTimeout)
	log.Printf("ExpiryTimeout:%d\n", Config.ExpiryTimeout)
}
