# tiny_redis 

There are a broker, a simple client for accessing broker and a number of nodes. The broker receives key(string)/value(string) pairs via HTTP and redirects data into in-memory cache on specific node via TCP.


# Broker

The broker supports RESTfull API.

Curl examples:
* curl -vXPOST http://localhost:42586/add -H "Content-Type: application/json" -d '{"key":"key1", "value":"1234", "props": {"TTL":5, "persistent":true}}' 
* curl -vXGET http://localhost:42586/show/{key}
* curl -vXPUT http://localhost:42586/update/{key} -H "Content-Type: application/json" -d '{"key":"key1", "value":"4321", "props": {"TTL":5, "persistent":true}}'
* curl -vXDELETE http://localhost:42586/delete/{key}
* curl -vGET http://localhost:42586/showkeys


# Nodes 

Node can be started via command node.exe <nodeID>. Nodes should be listed in config.json file.

Node is chosen by simple CRC32(key) % len(number of nodes). It's simple. More smart approaches are below:

Big Redis uses 16384 slots. Each node has its own range within these slots. Node is chosen by CRC16(key) % 16384 and hitting the specified range. It allows to add and remove nodes on runtime. See http://redis.io/topics/cluster-tutorial

Mongo-DB sharding: https://docs.mongodb.org/manual/core/sharding-shard-key/#shard-key

Sharding Pattern: https://msdn.microsoft.com/en-us/library/dn589797.aspx 

	 
# Network communication.

I'm using a bare TCP socket. Network connections are blocking. It makes a big impact on performance. 
Added a primitive reconnect logic between broker and nodes.
Network communication can be re-implemented for example by using ZeroMQ, adding monitoring and cluster control status.
For preventing memory allocation and deallocation of byte arrays and result in aggressive garbage collection we can use pre-allocated buffers. http://openmymind.net/Introduction-To-Go-Channels 

Another possible approaches (maybe deprecated):
	Network channels https://godoc.org/golang.org/x/exp/old/netchan
 

# In-memory cache

In-memory cache supports TTL and persistence for messages which are marked as TTL != 0 and set to true persistent option.
In-memory caches are based 
on mutex: http://openmymind.net/Do-More-In-Process-Caching https://github.com/patrickmn/go-cache
on channel: https://www.refheap.com/3009
	
My implementation is based on mutex. The cache doesn't support map and list. Example of proper implementation for any value type is here: https://github.com/patrickmn/go-cache 


# Config

Configuration file is config.json. It's possible to specify addresses of nodes and broker, broker external port, TTL checking period (seconds), data storing period (seconds).
{
    "ExternalPort":     42586,
    "ClientHostName":   "127.0.0.1",
    "nodes":            ["127.0.0.1:42587", "127.0.0.1:42588"],
    "diskiotimeout":    60,
    "expirytimeout":    60
}

TTL and persistence were implemented in naive way (by timer and locking the whole cache).


# Auth 

Auth is not implemented. Example is here http://shadynasty.biz/blog/2012/09/05/auth-and-sessions/


# Testing

There is just an example data_storage_test.go test file.


# Known bugs

Search pattern: //TODO: bug.


# Performance

Search pattern for issues: //TODO: Performance issue.

Client test results:
	1 cleint + http broker + node stub - 785 msg/sec.
	1 cleint + http broker + 1 node - 495 msg/sec.
	1 cleint + http broker + 2 node - 480 msg/sec.

Server test results:
	It's impossible to provide full performance test on Windows machine because of lack of resources.
	On server side Rate moving average was added. See log.Printf("--------------->>>>>>>>>>>>>>>>>>Rate %f\n", EMA) for analyzing rate.


# Code convention

I didn't care about it, but for example it's possible to check code with https://github.com/golang/lint